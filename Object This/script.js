 // this
// var a = 10;
// console.log(this.a);
 

// membuat object

// cara 1 - function declaration
// function halo()
// console.log(this);
// console.log('halo');
// }
// this.halo();

// cara 2 - object literal
var obj = {a : 10, nama : 'Muhammad'};
obj.halo = function(){
    console.log(this);
    console.log('halo');
}
obj.halo();


// cara 3 - constructor
function Halo(){
    console.log(this);
    console.log('halo');
}
var obj1 = new Halo();
var obj2 = new Halo();