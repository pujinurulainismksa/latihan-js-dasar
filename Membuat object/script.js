// membuat object
// object literal
  var mhs1 = {
    nama : "Abdullah Irsyadi",
    nrp : "033040007",
    email : "AbdullahIrsy@gmail.com",
    jurusan : "Teknik Pertanian"
}
//Function Declaration
function buatObjectMahasiswa(nama, nrp, email, jurusan){
    var mhs = {}
    mhs.nama = nama;
    mhs.nrp = nrp;
    mhs.email = email;
    mhs.jurusan = jurusan;
    return mhs;
}

var mhs2 = buatObjectMahasiswa('Irsyad maulana', '030450024', 'IrysdMln@gmail.com','Teknik Hukum')
// Constructor
function Mahasiswa(nama, nrp, email, jurusan){
    // var this = {};
    this.nama = nama;
    this.nrp = nrp;
    this.email = email;
    this.jurusan = jurusan;
    // retrun this;
}

var mhs3 = new Mahasiswa('Iqbal', '020134056', 'erkee@yahoo.com', 'Teknik Arsitek');












